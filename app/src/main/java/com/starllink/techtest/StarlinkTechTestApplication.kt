package com.starllink.techtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class StarlinkTechTestApplication : Application()