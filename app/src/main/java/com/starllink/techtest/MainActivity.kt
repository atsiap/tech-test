package com.starllink.techtest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.ui.Modifier
import com.starllink.features.roundup.RoundUpScreen
import com.starllink.techtest.ui.theme.StarllinkTechTestTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            StarllinkTechTestTheme {
                RoundUpScreen(Modifier)
            }
        }
    }
}