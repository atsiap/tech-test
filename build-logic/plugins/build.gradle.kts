plugins {
    `kotlin-dsl`
}

group = "com.starlink.techtest.buildlogic"

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

dependencies {
    compileOnly(libs.android.gradlePlugin)
    compileOnly(libs.kotlin.gradlePlugin)
}

gradlePlugin {
    plugins {
        register("androidLibraryPlugin") {
            id = "com.starlink.techtest.android.library"
            implementationClass = "com.starlink.techtest.plugins.AndroidLibraryPlugin"
        }
        register("androidApplicationPlugin") {
            id = "com.starlink.techtest.android.application"
            implementationClass = "com.starlink.techtest.plugins.AndroidApplicationPlugin"
        }
        register("androidLibraryComposePlugin") {
            id = "com.starlink.techtest.android.library.compose"
            implementationClass = "com.starlink.techtest.plugins.AndroidLibraryComposePlugin"
        }
        register("androidApplicationComposePlugin") {
            id = "com.starlink.techtest.android.application.compose"
            implementationClass = "com.starlink.techtest.plugins.AndroidApplicationComposePlugin"
        }
        register("androidHiltPlugin") {
            id = "com.starlink.techtest.android.hilt"
            implementationClass = "com.starlink.techtest.plugins.AndroidHiltPlugin"
        }
    }
}