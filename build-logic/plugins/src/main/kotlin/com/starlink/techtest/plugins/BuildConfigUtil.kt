package com.starlink.techtest.plugins

import com.android.build.api.dsl.LibraryDefaultConfig
import org.gradle.api.Project

/**
 * Helper utility to setup build config properties
 */
object BuildConfigUtil {

    fun Project.requiredBuildConfig(propertyName: String, config: LibraryDefaultConfig) {
        val dollar ="\$"
        val propertyValue = requireNotNull(project.properties[propertyName] as? String) {
            """
                Required property with name $propertyName is missing. Please append this property
                either thought local gradle.properties or ${dollar}HOME/.gradle/gradle.properties". 
                The later option is preferred when the nature of the information is either personal
                or confidential
            """.trimIndent()
        }

        config.buildConfigField("String", propertyName, propertyValue)
    }
}