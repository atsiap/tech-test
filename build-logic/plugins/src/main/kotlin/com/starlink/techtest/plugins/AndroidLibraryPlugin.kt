package com.starlink.techtest.plugins

import com.android.build.gradle.LibraryExtension
import com.starlink.techtest.plugins.configuration.configureKotlinAndroidLibrary
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

/**
 * Gradle plugin being used in the configuration of an Android Library
 */
class AndroidLibraryPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with (target) {
            with(pluginManager) {
                apply("com.android.library")
                apply("org.jetbrains.kotlin.android")
            }
            extensions.configure<LibraryExtension> {
                configureKotlinAndroidLibrary(this)
                defaultConfig.targetSdk = 33
            }
        }
    }
}