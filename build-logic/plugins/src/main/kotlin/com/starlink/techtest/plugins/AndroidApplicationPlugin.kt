package com.starlink.techtest.plugins

import com.android.build.api.dsl.ApplicationExtension
import com.starlink.techtest.plugins.configuration.configureKotlinAndroidApplication
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure

/**
 * Gradle plugin being used in the configuration of the Android Application.
 * This must be used only once per project.
 */
class AndroidApplicationPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with (target) {
            with(pluginManager) {
                apply("com.android.application")
                apply("org.jetbrains.kotlin.android")
            }
            extensions.configure<ApplicationExtension> {
                configureKotlinAndroidApplication(this)
            }
        }
    }
}