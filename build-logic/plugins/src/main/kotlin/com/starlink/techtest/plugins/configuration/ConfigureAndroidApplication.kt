package com.starlink.techtest.plugins.configuration

import com.android.build.api.dsl.ApplicationExtension
import org.gradle.api.Project

/**
 * Helper function which can configure an android application
 */
internal fun Project.configureKotlinAndroidApplication(
    applicationExtension: ApplicationExtension
) {
    with (applicationExtension) {
            namespace = "com.starllink.techtest"
            compileSdk = 33

            defaultConfig {
                applicationId = "com.starllink.techtest"
                minSdk = 23
                targetSdk = 33
                versionCode =  1
                versionName = "1.0"

                testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                vectorDrawables {
                    useSupportLibrary = true
                }
            }

            buildTypes {
                getByName("release") {
                    isMinifyEnabled = false
                    proguardFiles(
                        getDefaultProguardFile("proguard-android-optimize.txt"),
                        "proguard-rules.pro"
                    )
                }
            }
            configureCompileOptions()
            configureKotlinOptions()
            buildFeatures {
                compose = true
            }
            composeOptions {
                kotlinCompilerExtensionVersion = "1.3.2"
            }
            packagingOptions {
                resources {
                    excludes += "/META-INF/{AL2.0,LGPL2.1}"
                }
            }
    }
}