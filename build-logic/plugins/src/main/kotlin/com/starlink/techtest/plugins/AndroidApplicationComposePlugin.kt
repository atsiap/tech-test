package com.starlink.techtest.plugins

import com.android.build.api.dsl.ApplicationExtension
import com.starlink.techtest.plugins.configuration.configureCompose
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

/**
 * Gradle plugin being used in the configuration of Jetpack Compose within the Android Application
 */
class AndroidApplicationComposePlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply("com.android.application")
            val extension = extensions.getByType<ApplicationExtension>()
            configureCompose(extension)
        }
    }
}