package com.starlink.techtest.plugins.configuration

import com.android.build.api.dsl.CommonExtension
import org.gradle.api.Project
import org.gradle.api.artifacts.VersionCatalog
import org.gradle.api.artifacts.VersionCatalogsExtension
import org.gradle.kotlin.dsl.DependencyHandlerScope
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.getByType

/**
 * Helper function to setup the jetpack compose
 */
internal fun Project.configureCompose(
    commonExtension: CommonExtension<*, *, *, *>,
) {
    val libs = extensions.getByType<VersionCatalogsExtension>().named("libs")

    with (commonExtension) {
        buildFeatures {
            compose = true
        }

        composeOptions {
            kotlinCompilerExtensionVersion = libs.findVersion("androidComposeCompiler").get().toString()
        }

        dependencies {
            val bom = libs.findLibrary("android-compose-bom").get()
            add("implementation", platform(bom))
            add("androidTestImplementation", platform(bom))

            addLibrary(libs, "implementation", "android-compose-ui")
            addLibrary(libs, "implementation", "android-compose-ui-tooling-preview")
            addLibrary(libs, "debugImplementation", "android-compose-ui-tooling")
            addLibrary(libs, "debugImplementation", "android-compose-ui-test-manifest")
            addLibrary(libs, "implementation", "android-compose-material3")
        }
    }
}

private fun DependencyHandlerScope.addLibrary(
    versionCatalog: VersionCatalog,
    configurationName: String,
    libraryName: String
) {
    add(configurationName, versionCatalog.findLibrary(libraryName).get())
}
