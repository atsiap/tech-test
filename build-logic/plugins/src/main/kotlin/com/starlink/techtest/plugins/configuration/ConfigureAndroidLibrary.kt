package com.starlink.techtest.plugins.configuration

import com.android.build.gradle.LibraryExtension
import com.starlink.techtest.plugins.configuration.configureCompileOptions
import com.starlink.techtest.plugins.configuration.configureKotlinOptions
import org.gradle.api.Project

/**
 * Helper function which can configure an android library
 */
internal fun Project.configureKotlinAndroidLibrary(
    libraryExtension: LibraryExtension
) {
    with(libraryExtension) {
        compileSdk = 33

        defaultConfig {
            minSdk = 23
            consumerProguardFiles("consumer-rules.pro")
            testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        }

        buildTypes {
            getByName("release") {
                isMinifyEnabled = false
                proguardFiles(
                    getDefaultProguardFile("proguard-android-optimize.txt"),
                    "proguard-rules.pro"
                )
            }
        }

        configureCompileOptions()
        configureKotlinOptions()
    }
}