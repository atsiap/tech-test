package com.starlink.techtest.plugins

import com.android.build.gradle.LibraryExtension
import com.starlink.techtest.plugins.configuration.configureCompose
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

/**
 * Gradle plugin being used in the configuration of Jetpack Compose within an Android Library
 */
class AndroidLibraryComposePlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply("com.android.library")
            val extension = extensions.getByType<LibraryExtension>()
            configureCompose(extension)
        }
    }
}