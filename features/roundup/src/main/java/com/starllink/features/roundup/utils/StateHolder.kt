package com.starllink.features.roundup.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filterIsInstance
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update


/**
 * Utility class which allows us to keep temporal state inside the ViewModel
 */
class StateHolder<T> private constructor(
    initialValue: Box<T>
) {
    constructor() : this(Box.Empty())

    constructor(initialValue: T) : this(Box.Value(initialValue))

    private val mutableStateFlow = MutableStateFlow(initialValue)

    val flow: Flow<T>
        get() = mutableStateFlow.filterIsInstance<Box.Value<T>>().map { it.data }

    fun update(value: T) {
        mutableStateFlow.update { Box.Value(value) }
    }

    private sealed interface Box<T> {
        class Empty<T> : Box<T>

        data class Value<T>(val data: T) : Box<T>
    }
}