package com.starllink.features.roundup

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel

@Composable
fun RoundUpScreen(
    modifier: Modifier,
    viewModel: RoundUpViewModel = hiltViewModel()
) {
    val state by viewModel.state.collectAsState()
    Surface(
        modifier = modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        RoundUpScreenContent(
            state,
            viewModel::refresh,
            viewModel::createSavingsGoal,
            viewModel::deleteSavingsGoal,
            viewModel::deleteInfightOperationMessage,
            viewModel::moveMoneyToSavingsGoal
        )
    }
}

@Composable
private fun RoundUpScreenContent(
    state: RoundUpState,
    onRefresh: () -> Unit,
    createSavingsGoal: (String) -> Unit,
    deleteSavingsGoal: (String, String) -> Unit,
    onMessageShown: (RoundUpInflightOperationMessage) -> Unit,
    moveMoneyToSavingsGoal: (String, String, Int) -> Unit
) {
    Column(modifier = Modifier.padding(32.dp)) {
        when (state) {
            is RoundUpState.Failure -> RoundUpScreenErrorContent(state.error, onRefresh)
            is RoundUpState.Loading -> LoadingContent()
            is RoundUpState.Success -> RoundUpContent(
                state = state,
                createSavingsGoal = { createSavingsGoal(state.accountUid) },
                deleteSavingsGoal = {
                    deleteSavingsGoal(
                        state.accountUid,
                        state.savingsGoal?.savingsGoalUid ?: ""
                    )
                },
                onMessageShown = onMessageShown,
                moveMoneyToSavingsGoal = {
                    moveMoneyToSavingsGoal(
                        state.accountUid,
                        // guarded by the UI component
                        state.savingsGoal!!.savingsGoalUid,
                        state.roundAmount.amount
                    )
                }
            )
        }
    }
}

@Composable
private fun LoadingContent() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        CircularProgressIndicator()
    }
}