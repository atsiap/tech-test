package com.starllink.features.roundup

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.starllink.domain.core.enqueue
import com.starllink.domain.models.FeedResource
import com.starllink.domain.models.SavingsGoal
import com.starllink.domain.usecase.FeedUseCase
import com.starllink.domain.utils.Result
import com.starllink.domain.utils.asResult
import com.starllink.domain.utils.doOnError
import com.starllink.features.roundup.utils.StateHolder
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import java.util.UUID
import javax.inject.Inject

@HiltViewModel
class RoundUpViewModel @Inject constructor(
    private val feedUseCase: FeedUseCase,
    private val operationManager: RoundUpOperationManager,
) : ViewModel() {

    private val savingsGoalStateHolder = StateHolder<SavingsGoal?>()
    private val feedFlow = MutableSharedFlow<Result<FeedResource>>()

    val state: StateFlow<RoundUpState> = roundUpFlow()
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(),
            initialValue = RoundUpState.Loading
        )

    init {
        refresh()
    }

    fun refresh() {
        feedUseCase.enqueue(Unit)
            .onEach { savingsGoalStateHolder.update(it.savingsGoal) }
            .asResult()
            .doOnError { savingsGoalStateHolder.update(null) }
            .onEach(feedFlow::emit)
            .launchIn(viewModelScope)
    }

    fun createSavingsGoal(accountUid: String) {
        viewModelScope.launch {
            operationManager.createSavingsGoal(accountUid, savingsGoalStateHolder::update)
        }
    }

    fun deleteSavingsGoal(accountUid: String, savingsGoalUid: String) {
        viewModelScope.launch {
            operationManager.deleteSavingsGoal(
                accountUid, savingsGoalUid, savingsGoalStateHolder::update
            )
        }
    }

    fun deleteInfightOperationMessage(message: RoundUpInflightOperationMessage) {
        operationManager.deleteInfightOperationMessage(message)
    }

    fun moveMoneyToSavingsGoal(
        accountUid: String,
        savingsGoalUid: String,
        amount: Int
    ) {
        viewModelScope.launch {
            operationManager.moveMoneyToSavingsGoal(
                accountUid, savingsGoalUid, UUID.randomUUID().toString(), amount
            )
        }
    }

    private fun roundUpFlow(): Flow<RoundUpState> = combine(
        feedFlow,
        savingsGoalStateHolder.flow,
        operationManager.flow,
    ) { feedResult, savingsGoal, roundUpOperation ->
        when (feedResult) {
            is Result.Success -> RoundUpState.Success(
                accountUid = feedResult.data.accountUid,
                defaultCategory = feedResult.data.defaultCategory,
                roundAmount = feedResult.data.roundAmount,
                savingsGoal = savingsGoal,
                inflightOperations = roundUpOperation.inflightOperation,
                inflightOperationMessage = roundUpOperation.inflightOperationMessage
            )

            is Result.Error -> RoundUpState.Failure(feedResult.exception)
            is Result.Loading -> RoundUpState.Loading
        }
    }
}