package com.starllink.features.roundup

import com.starllink.domain.models.RoundAmount
import com.starllink.domain.models.SavingsGoal

sealed interface RoundUpState {

    object Loading : RoundUpState

    data class Success(
        val accountUid: String,
        val defaultCategory: String,
        val roundAmount: RoundAmount,
        val savingsGoal: SavingsGoal?,
        val inflightOperations: RoundUpInflightOperation,
        val inflightOperationMessage: RoundUpInflightOperationMessage?
    ) : RoundUpState

    data class Failure(val error: Throwable) : RoundUpState
}

enum class RoundUpInflightOperationMessage {
    FAILED_TO_CREATE_SAVINGS_GOAL,
    FAILED_TO_DELETE_SAVINGS_GOAL,
    FAILED_TO_MOVE_MONEY_SAVINGS_GOAL,
    SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL
}

enum class RoundUpInflightOperation {
    CREATE_SAVINGS_GOAL,
    DELETE_SAVINGS_GOAL,
    MOVE_MONEY_SAVINGS_GOAL,
    IDLE
}