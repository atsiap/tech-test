package com.starllink.features.roundup

import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

@Composable
internal fun ActionButtonOutlined(text: String, onClick: () -> Unit) {
    OutlinedButton(onClick = onClick) {
        Text(text = text)
    }
}