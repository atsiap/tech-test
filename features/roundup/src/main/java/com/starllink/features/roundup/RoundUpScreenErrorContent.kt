package com.starllink.features.roundup

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.starllink.domain.exceptions.FeedResourceException

@Composable
internal fun RoundUpScreenErrorContent(throwable: Throwable, onRefresh: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val errorMsg = when (throwable) {
            is FeedResourceException -> stringResource(throwable.errorType.toStringResource())
            else -> throwable.message ?: ""
        }

        Text(
            text = stringResource(id = R.string.error_text),
            style = MaterialTheme.typography.headlineMedium
        )

        Spacer(4.dp)

        Text(
            text = errorMsg,
            style = MaterialTheme.typography.headlineMedium
        )

        Spacer(16.dp)

        ActionButtonOutlined(onClick = onRefresh, text = stringResource(id = R.string.refresh))
    }
}

private fun FeedResourceException.ErrorType.toStringResource() = when (this) {
    FeedResourceException.ErrorType.ACCOUNT_NOT_FOUND -> R.string.error_account_not_found
    FeedResourceException.ErrorType.ACCOUNT_FAILED -> R.string.error_account_loading_failed
    FeedResourceException.ErrorType.ROUND_UP_FAILED -> R.string.error_round_up_loading_failed
    FeedResourceException.ErrorType.SAVINGS_GOAL_FAILED -> R.string.error_savings_goal_loading_failed
}