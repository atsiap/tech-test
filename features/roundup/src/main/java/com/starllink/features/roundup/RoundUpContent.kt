package com.starllink.features.roundup

import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.starllink.domain.models.SavingsGoal
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_CREATE_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_DELETE_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_MOVE_MONEY_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL

@Composable
internal fun RoundUpContent(
    state: RoundUpState.Success,
    createSavingsGoal: () -> Unit,
    deleteSavingsGoal: () -> Unit,
    onMessageShown: (RoundUpInflightOperationMessage) -> Unit,
    moveMoneyToSavingsGoal: () -> Unit
) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        EntryLabel(
            text = stringResource(id = R.string.account_id, state.accountUid)
        )

        EntryLabel(
            text = stringResource(id = R.string.category_id, state.defaultCategory)
        )

        SavingsGoalFailure(state.inflightOperationMessage, onMessageShown)
        SavingsGoalEntry(
            state.savingsGoal,
            state.inflightOperations,
            createSavingsGoal,
            deleteSavingsGoal
        )

        val roundUpAmount = state.roundAmount.amount / 100f
        EntryLabel(
            text = stringResource(id = R.string.round_up_amount_for_the_week, roundUpAmount),
        )

        MoveMoneyToSavingsGoal(
            state.savingsGoal,
            state.inflightOperations,
            moveMoneyToSavingsGoal
        )
    }
}

@Composable
private fun SavingsGoalEntry(
    savingsGoal: SavingsGoal?,
    inflightOperations: RoundUpInflightOperation,
    createSavingsGoal: () -> Unit,
    deleteSavingsGoal: () -> Unit
) {
    if (inflightOperations == RoundUpInflightOperation.CREATE_SAVINGS_GOAL) {
        EntryLabel(text = stringResource(id = R.string.savings_goal_creation_in_progress))
    } else if (inflightOperations == RoundUpInflightOperation.DELETE_SAVINGS_GOAL) {
        EntryLabel(text = stringResource(id = R.string.savings_goal_deletion_in_progress))
    } else if (savingsGoal == null) {
        EntryLabel(
            text = stringResource(id = R.string.savings_goal_does_not_exist),
        )

        ActionButtonOutlined(
            onClick = createSavingsGoal,
            text = stringResource(id = R.string.savings_goal_create)
        )
    } else {
        if (!savingsGoal.isSingle) {
            EntryLabel(
                text = stringResource(id = R.string.savings_goal_multiple),
            )
        }
        EntryLabel(
            text = stringResource(id = R.string.savings_goal_id, savingsGoal.savingsGoalUid),
        )
        ActionButtonOutlined(
            onClick = deleteSavingsGoal,
            text = stringResource(id = R.string.savings_goal_delete)
        )
    }
}

@Composable
private fun SavingsGoalFailure(
    inlfightOperationMessage: RoundUpInflightOperationMessage?,
    onMessageShown: (RoundUpInflightOperationMessage) -> Unit,
) {
    inlfightOperationMessage?.let { message ->
        val currentContext = LocalContext.current
        val errorMessage = stringResource(message.toErrorMessage())
        LaunchedEffect(message) {
            Toast.makeText(currentContext, errorMessage, Toast.LENGTH_LONG).show()
            onMessageShown(message)
        }
    }
}

@Composable
private fun MoveMoneyToSavingsGoal(
    savingsGoal: SavingsGoal?,
    inflightOperations: RoundUpInflightOperation,
    moveMoneyToSavingsGoal: () -> Unit
) {
    if (savingsGoal != null) {
        if (inflightOperations == RoundUpInflightOperation.MOVE_MONEY_SAVINGS_GOAL) {
            EntryLabel(
                text = stringResource(id = R.string.savings_goal_move_money_in_progress),
            )
        } else if (inflightOperations == RoundUpInflightOperation.IDLE) {
            ActionButtonOutlined(
                onClick = moveMoneyToSavingsGoal,
                text = stringResource(id = R.string.savings_goal_move_money)
            )
        }
    }
}

@Composable
private fun EntryLabel(text: String) {
    Text(
        modifier = Modifier.fillMaxWidth(),
        text = text,
        textAlign = TextAlign.Start,
        style = MaterialTheme.typography.bodyLarge
    )
    Spacer(2.dp)
}

private fun RoundUpInflightOperationMessage.toErrorMessage() = when (this) {
    FAILED_TO_CREATE_SAVINGS_GOAL -> R.string.savings_goal_failed_to_be_created
    FAILED_TO_DELETE_SAVINGS_GOAL -> R.string.savings_goal_failed_to_be_deleted
    FAILED_TO_MOVE_MONEY_SAVINGS_GOAL -> R.string.savings_goal_move_money_failed
    SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL -> R.string.savings_goal_move_money_succeed
}