package com.starllink.features.roundup.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

/**
 * Utility class which keeps items that are going to be consumed by the UI.
 * This is useful for one time events such as error messages.
 *
 * On the other hand, [StateHolder] must be used when we want to keep an entiry as a State
 * rather than a collection of 1 off events.
 */
class UIMessageHolder<T> {

    private val mutableStateFlow = MutableStateFlow<List<T>>(emptyList())

    val flow: Flow<T?>
        get() = mutableStateFlow.map { it.firstOrNull() }.distinctUntilChanged()

    fun emitItem(item: T) {
        mutableStateFlow.update { it + item }
    }

    fun clearItem(item: T) {
        mutableStateFlow.update { current -> current.filterNot { it == item } }
    }
}