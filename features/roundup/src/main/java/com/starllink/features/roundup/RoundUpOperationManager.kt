package com.starllink.features.roundup

import com.starllink.domain.models.SavingsGoal
import com.starllink.domain.usecase.CreateSavingsGoalUseCase
import com.starllink.domain.usecase.DeleteSavingsGoalUseCase
import com.starllink.domain.usecase.MoveMoneyUseCase
import com.starllink.domain.utils.runCatchingSuspend
import com.starllink.features.roundup.RoundUpInflightOperationMessage.SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL
import com.starllink.features.roundup.utils.StateHolder
import com.starllink.features.roundup.utils.UIMessageHolder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class RoundUpOperationManager @Inject constructor(
    private val createSavingsGoalUseCase: CreateSavingsGoalUseCase,
    private val deleteSavingsGoalUseCase: DeleteSavingsGoalUseCase,
    private val moveMoneyUseCase: MoveMoneyUseCase
) {
    /**
     * State around the inflight operations (Create Savings Goal, Delete Savings Goal,
     * Transfer round up amount)
     */
    private val inflightOperationHolder = StateHolder(RoundUpInflightOperation.IDLE)

    /**
     * One-off messages which are being displayed by the UI when there is a status update from
     * the inflight operations (Create Savings Goal, Delete Savings Goal,
     * Transfer round up amount)
     */
    private val inflightOperationMessages = UIMessageHolder<RoundUpInflightOperationMessage>()

    val flow: Flow<RoundUpOperation> = combine(
        inflightOperationHolder.flow,
        inflightOperationMessages.flow
    ) { inflightOperations, inflightOperationMessage ->
        RoundUpOperation(
            inflightOperations,
            inflightOperationMessage
        )
    }

    suspend fun createSavingsGoal(accountUid: String, onSuccess: (SavingsGoal?) -> Unit) {
        executeSavingsGoalOperation(
            RoundUpInflightOperation.CREATE_SAVINGS_GOAL,
            RoundUpInflightOperationMessage.FAILED_TO_CREATE_SAVINGS_GOAL,
            onSuccess,
        ) { createSavingsGoalUseCase.execute(accountUid) }
    }

    suspend fun deleteSavingsGoal(accountUid: String, savingsGoalUid: String, onSuccess: (SavingsGoal?) -> Unit) {
        executeSavingsGoalOperation(
            RoundUpInflightOperation.DELETE_SAVINGS_GOAL,
            RoundUpInflightOperationMessage.FAILED_TO_DELETE_SAVINGS_GOAL,
            onSuccess,
        ) {
            deleteSavingsGoalUseCase.execute(
                DeleteSavingsGoalUseCase.Params(accountUid, savingsGoalUid)
            )
            null
        }
    }

    suspend fun moveMoneyToSavingsGoal(
        accountUid: String,
        savingsGoalUid: String,
        transactionId: String,
        amount: Int
    ) {
        executeSavingsGoalOperation(
            RoundUpInflightOperation.MOVE_MONEY_SAVINGS_GOAL,
            RoundUpInflightOperationMessage.FAILED_TO_MOVE_MONEY_SAVINGS_GOAL,
            { inflightOperationMessages.emitItem(SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL) }
        ) {
            val params = MoveMoneyUseCase.Params(
                accountUid = accountUid,
                savingsGoalUid = savingsGoalUid,
                transactionId = transactionId,
                amount = amount
            )
            moveMoneyUseCase.execute(params)
            null
        }
    }

    private suspend fun executeSavingsGoalOperation(
        type: RoundUpInflightOperation,
        failedOperationMessage: RoundUpInflightOperationMessage,
        onSuccess: (SavingsGoal?) -> Unit,
        operation: suspend () -> SavingsGoal?,
    ) {
        val result = runCatchingSuspend {
            inflightOperationHolder.update(type)
            val savingsGoal = operation()
            onSuccess(savingsGoal)
        }

        inflightOperationHolder.update(RoundUpInflightOperation.IDLE)
        if (result.isFailure) {
            inflightOperationMessages.emitItem(failedOperationMessage)
        }
    }

    fun deleteInfightOperationMessage(message: RoundUpInflightOperationMessage) {
        inflightOperationMessages.clearItem(message)
    }

}

data class RoundUpOperation(
    val inflightOperation: RoundUpInflightOperation,
    val inflightOperationMessage: RoundUpInflightOperationMessage?,
)