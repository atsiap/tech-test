package com.starllink.features.roundup.utils

import app.cash.turbine.ReceiveTurbine
import org.junit.Assert.assertEquals

suspend fun <T> ReceiveTurbine<T>.verifyStates(vararg states: T) {
    for (state in states) {
        assertEquals(
            state, awaitItem()
        )
    }
}