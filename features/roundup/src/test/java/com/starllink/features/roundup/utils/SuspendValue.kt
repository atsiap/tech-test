package com.starllink.features.roundup.utils

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first

/**
 * Utility class which keeps a value for a suspend function.
 * This is usually helpful when you want to emit different values
 * from a suspend function
 */
class SuspendValue<T> {

    private val state = MutableStateFlow<Box<T>>(Box.Empty())

    fun set(value: T) {
        state.value = Box.Value(value)
    }

    fun set(exception: Throwable) {
        state.value = Box.Failure(exception)
    }

    suspend fun await(): T {
        val boxValue = state.filter { it !is Box.Empty}.first()
        state.value = Box.Empty()

        return when (boxValue) {
            is Box.Empty -> error("Should not happen")
            is Box.Failure -> throw boxValue.error
            is Box.Value -> boxValue.data
        }
    }

    private sealed interface Box<T> {

        data class Value<T>(val data: T) : Box<T>

        data class Failure<T>(val error: Throwable) : Box<T>

        class Empty<T> : Box<T>
    }
}