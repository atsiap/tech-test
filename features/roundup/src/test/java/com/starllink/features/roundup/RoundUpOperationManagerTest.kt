package com.starllink.features.roundup

import app.cash.turbine.ReceiveTurbine
import app.cash.turbine.testIn
import com.starllink.domain.models.SavingsGoal
import com.starllink.domain.usecase.CreateSavingsGoalUseCase
import com.starllink.domain.usecase.DeleteSavingsGoalUseCase
import com.starllink.domain.usecase.MoveMoneyUseCase
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_CREATE_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_DELETE_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.FAILED_TO_MOVE_MONEY_SAVINGS_GOAL
import com.starllink.features.roundup.RoundUpInflightOperationMessage.SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL
import com.starllink.features.roundup.utils.verifyStates
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.IOException

class RoundUpOperationManagerTest {

    private val createSavingsGoalUseCase = mock<CreateSavingsGoalUseCase>()
    private val deleteSavingsGoalUseCase = mock<DeleteSavingsGoalUseCase>()
    private val moveMoneyUseCase = mock<MoveMoneyUseCase>()

    private val manager = RoundUpOperationManager(
        createSavingsGoalUseCase = createSavingsGoalUseCase,
        deleteSavingsGoalUseCase = deleteSavingsGoalUseCase,
        moveMoneyUseCase = moveMoneyUseCase
    )

    @Test
    fun `verify initial state`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        assertEquals(
            RoundUpOperation(RoundUpInflightOperation.IDLE, null), stateObserver.awaitItem()
        )

        stateObserver.ensureAllEventsConsumed()
    }

    // region create savings goal test
    @Test
    fun `verify savings goal created successfully`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        val savingsGoal = SavingsGoal("savingsGoalUid", true)
        whenever(createSavingsGoalUseCase.execute(accountUid)).thenReturn(savingsGoal)

        stateObserver.verifyIdleState()

        manager.createSavingsGoal(accountUid) {
            assertEquals(savingsGoal, it)
        }

        // verify in progress state
        stateObserver.verifyStates(
            RoundUpOperation(RoundUpInflightOperation.CREATE_SAVINGS_GOAL, null)
        )

        // verify operation manager returned to initial state after the operation is completed
        stateObserver.verifyIdleState()
        stateObserver.ensureAllEventsConsumed()
    }

    @Test
    fun `verify savings goal created failed`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        whenever(createSavingsGoalUseCase.execute(accountUid)).thenAnswer {
            throw IOException("error")
        }

        stateObserver.verifyIdleState()

        var callbackDidNotTrigger = false
        manager.createSavingsGoal(accountUid) { callbackDidNotTrigger = true }

        stateObserver.verifyStates(
            // verify in progress state
            RoundUpOperation(RoundUpInflightOperation.CREATE_SAVINGS_GOAL, null),
            // verify in idle state
            RoundUpOperation(RoundUpInflightOperation.IDLE, null),
            // verify in error state
            RoundUpOperation(RoundUpInflightOperation.IDLE, FAILED_TO_CREATE_SAVINGS_GOAL)
        )

        assertEquals(false, callbackDidNotTrigger)

        manager.deleteInfightOperationMessage(FAILED_TO_CREATE_SAVINGS_GOAL)

        stateObserver.verifyIdleState()

        stateObserver.ensureAllEventsConsumed()
    }

    // endregion

    // region delete savings goal test

    @Test
    fun `verify savings goal deleted successfully`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        val savingsGoalUid = "savingsGoalUid"
        val params = DeleteSavingsGoalUseCase.Params(accountUid, savingsGoalUid)
        whenever(deleteSavingsGoalUseCase.execute(params)).thenReturn(Unit)

        stateObserver.verifyIdleState()

        manager.deleteSavingsGoal(accountUid, savingsGoalUid) {
            assertEquals(null, it)
        }

        // verify in progress state
        stateObserver.verifyStates(
            RoundUpOperation(RoundUpInflightOperation.DELETE_SAVINGS_GOAL, null)
        )

        // verify operation manager returned to initial state after the operation is completed
        stateObserver.verifyIdleState()
        stateObserver.ensureAllEventsConsumed()
    }

    @Test
    fun `verify savings goal deleted failed`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        val savingsGoalUid = "savingsGoalUid"
        val params = DeleteSavingsGoalUseCase.Params(accountUid, savingsGoalUid)
        whenever(deleteSavingsGoalUseCase.execute(params)).thenAnswer {
            throw IOException("error")
        }

        stateObserver.verifyIdleState()

        var callbackDidNotTrigger = false
        manager.deleteSavingsGoal(accountUid, savingsGoalUid) {
            callbackDidNotTrigger = true
        }

        stateObserver.verifyStates(
            // verify in progress state
            RoundUpOperation(RoundUpInflightOperation.DELETE_SAVINGS_GOAL, null),
            // verify idle state
            RoundUpOperation(RoundUpInflightOperation.IDLE, null),
            // verify in error state
            RoundUpOperation(RoundUpInflightOperation.IDLE, FAILED_TO_DELETE_SAVINGS_GOAL)
        )

        assertEquals(false, callbackDidNotTrigger)

        manager.deleteInfightOperationMessage(FAILED_TO_DELETE_SAVINGS_GOAL)

        stateObserver.verifyIdleState()

        stateObserver.ensureAllEventsConsumed()
    }

    // endregion

    // region move money to savings goal test

    @Test
    fun `verify move money to savings goal successfully`() = runTest {
        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        val savingsGoalUid = "savingsGoalUid"
        val transactionUid = "transactionUid"
        val amount = 123
        val params = MoveMoneyUseCase.Params(accountUid, savingsGoalUid, transactionUid, amount)
        whenever(moveMoneyUseCase.execute(params)).thenReturn(Unit)

        stateObserver.verifyIdleState()

        manager.moveMoneyToSavingsGoal(accountUid, savingsGoalUid, transactionUid, amount)

        // verify in progress state
        stateObserver.verifyStates(
            RoundUpOperation(RoundUpInflightOperation.MOVE_MONEY_SAVINGS_GOAL, null),
            RoundUpOperation(
                RoundUpInflightOperation.MOVE_MONEY_SAVINGS_GOAL,
                SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL
            ),
            RoundUpOperation(
                RoundUpInflightOperation.IDLE,
                SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL
            )
        )

        manager.deleteInfightOperationMessage(SUCCESSFULLY_MONEY_MOVED_TO_SAVINGS_GOAL)

        stateObserver.verifyIdleState()

        stateObserver.ensureAllEventsConsumed()
    }

    @Test
    fun `verify move money to savings goal failed`() = runTest {

        val stateObserver = manager.flow.testIn(backgroundScope)

        val accountUid = "accountUid"
        val savingsGoalUid = "savingsGoalUid"
        val transactionUid = "transactionUid"
        val amount = 123
        val params = MoveMoneyUseCase.Params(accountUid, savingsGoalUid, transactionUid, amount)
        whenever(moveMoneyUseCase.execute(params)).thenAnswer {
            throw IOException("error")
        }

        stateObserver.verifyIdleState()

        manager.moveMoneyToSavingsGoal(accountUid, savingsGoalUid, transactionUid, amount)

        stateObserver.verifyStates(
            // verify in progress state
            RoundUpOperation(RoundUpInflightOperation.MOVE_MONEY_SAVINGS_GOAL, null),
            // verify idle state
            RoundUpOperation(RoundUpInflightOperation.IDLE, null),
            // verify in error state
            RoundUpOperation(RoundUpInflightOperation.IDLE, FAILED_TO_MOVE_MONEY_SAVINGS_GOAL)
        )

        manager.deleteInfightOperationMessage(FAILED_TO_MOVE_MONEY_SAVINGS_GOAL)

        stateObserver.verifyIdleState()

        stateObserver.ensureAllEventsConsumed()
    }

    // endregion

    private suspend fun ReceiveTurbine<RoundUpOperation>.verifyIdleState() {
        verifyStates(RoundUpOperation(RoundUpInflightOperation.IDLE, null))
    }
}