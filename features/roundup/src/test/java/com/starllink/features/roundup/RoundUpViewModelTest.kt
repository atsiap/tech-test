package com.starllink.features.roundup

import app.cash.turbine.testIn
import com.starllink.domain.models.FeedResource
import com.starllink.domain.models.SavingsGoal
import com.starllink.domain.usecase.FeedUseCase
import com.starllink.features.roundup.fixtures.RoundUpViewModelFixtures.feedResource
import com.starllink.features.roundup.fixtures.RoundUpViewModelFixtures.feedResourceWithSavingsGoal
import com.starllink.features.roundup.fixtures.RoundUpViewModelFixtures.savingsGoal
import com.starllink.features.roundup.fixtures.RoundUpViewModelFixtures.successState
import com.starllink.features.roundup.fixtures.RoundUpViewModelFixtures.successStateWithSavingsGoal
import com.starllink.features.roundup.utils.MainDispatcherRule
import com.starllink.features.roundup.utils.SuspendValue
import com.starllink.features.roundup.utils.verifyStates
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.eq
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import org.mockito.stubbing.OngoingStubbing

class RoundUpViewModelTest {

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private val operationManager = mock<RoundUpOperationManager> {
        on { flow } doReturn flowOf(RoundUpOperation(RoundUpInflightOperation.IDLE, null))
    }

    private val suspendValue = SuspendValue<FeedResource>()
    private val feedUseCase = mock<FeedUseCase> {
        onBlocking { execute(Unit) } doSuspendableAnswer { suspendValue.await() }
    }

    private lateinit var viewModel: RoundUpViewModel

    @Before
    fun before() {
        viewModel = RoundUpViewModel(
            feedUseCase = feedUseCase,
            operationManager = operationManager
        )
    }

    @Test
    fun `verify savings goal`() = runTest {
        val stateObserver = viewModel.state.testIn(backgroundScope)

        suspendValue.set(feedResource)

        whenever(operationManager.createSavingsGoal(eq(feedResource.accountUid), any()))
            .mockSavingGoalCallback(savingsGoal, 1)

        viewModel.createSavingsGoal(feedResource.accountUid)

        verify(operationManager).createSavingsGoal(eq(feedResource.accountUid), any())

        stateObserver.verifyStates(
            RoundUpState.Loading,
            successState,
            successState.copy(savingsGoal = savingsGoal)
        )

        stateObserver.ensureAllEventsConsumed()
    }

    @Test
    fun `verify delete savings goal`() = runTest {
        val stateObserver = viewModel.state.testIn(backgroundScope)
        suspendValue.set(feedResourceWithSavingsGoal)

        whenever(
            operationManager.deleteSavingsGoal(
                eq(feedResource.accountUid),
                eq(savingsGoal.savingsGoalUid),
                any()
            )
        ).mockSavingGoalCallback(null, 2)

        viewModel.deleteSavingsGoal(feedResource.accountUid, savingsGoal.savingsGoalUid)

        verify(operationManager).deleteSavingsGoal(
            eq(feedResource.accountUid),
            eq(savingsGoal.savingsGoalUid),
            any()
        )

        stateObserver.verifyStates(
            RoundUpState.Loading,
            successStateWithSavingsGoal,
            successState
        )

        stateObserver.ensureAllEventsConsumed()
    }

    @Test
    fun `verify delete inflight operations message`() {
        viewModel.deleteInfightOperationMessage(
            RoundUpInflightOperationMessage.FAILED_TO_MOVE_MONEY_SAVINGS_GOAL
        )

        verify(operationManager).deleteInfightOperationMessage(
            RoundUpInflightOperationMessage.FAILED_TO_MOVE_MONEY_SAVINGS_GOAL
        )
    }

    @Test
    fun `verify refresh method`() = runTest {
        val stateObserver = viewModel.state.testIn(backgroundScope)

        // Coroutines are recreating the exception under the hood using reflection
        // so we need to use a special exception for this case
        val exception = RuntimeException()

        // when the FeedUseCase returns an error
        suspendValue.set(exception)
        stateObserver.verifyStates(
            RoundUpState.Loading,
            RoundUpState.Failure(exception)
        )

        // successful value when retrying
        suspendValue.set(feedResource)

        viewModel.refresh()

        stateObserver.verifyStates(
            RoundUpState.Loading,
            successState,
        )

        stateObserver.ensureAllEventsConsumed()
    }

    @Suppress("UNCHECKED_CAST")
    private fun OngoingStubbing<*>.mockSavingGoalCallback(
        savingsGoal: SavingsGoal?,
        parameterIndex: Int
    ) = thenAnswer { invocation ->
        val argument = invocation.arguments[parameterIndex] as (SavingsGoal?) -> Unit
        argument.invoke(savingsGoal)
    }
}