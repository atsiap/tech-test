package com.starllink.features.roundup.fixtures

import com.starllink.domain.models.FeedResource
import com.starllink.domain.models.RoundAmount
import com.starllink.domain.models.SavingsGoal
import com.starllink.features.roundup.RoundUpInflightOperation
import com.starllink.features.roundup.RoundUpState

object RoundUpViewModelFixtures {

    val feedResource = FeedResource(
        accountUid = "accountUid",
        defaultCategory = "defaultCategory",
        roundAmount = RoundAmount(123),
        savingsGoal = null,
    )

    val savingsGoal = SavingsGoal("savingsGoalUid", true)

    val feedResourceWithSavingsGoal = FeedResource(
        accountUid = "accountUid",
        defaultCategory = "defaultCategory",
        roundAmount = RoundAmount(123),
        savingsGoal = savingsGoal,
    )

    val successState = RoundUpState.Success(
        accountUid = feedResource.accountUid,
        defaultCategory = feedResource.defaultCategory,
        roundAmount = feedResource.roundAmount,
        savingsGoal = null,
        inflightOperations = RoundUpInflightOperation.IDLE,
        inflightOperationMessage = null
    )

    val successStateWithSavingsGoal = RoundUpState.Success(
        accountUid = feedResource.accountUid,
        defaultCategory = feedResource.defaultCategory,
        roundAmount = feedResource.roundAmount,
        savingsGoal = savingsGoal,
        inflightOperations = RoundUpInflightOperation.IDLE,
        inflightOperationMessage = null
    )

}