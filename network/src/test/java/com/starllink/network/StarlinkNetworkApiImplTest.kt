package com.starllink.network

import com.starllink.network.model.NetworkAccounts
import com.starllink.network.model.NetworkFeedItems
import com.starllink.network.model.NetworkSavingsGoal
import com.starllink.network.model.NetworkSavingsGoals
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import com.starllink.network.model.request.NetworkMoveMoneyAmount
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import com.starllink.network.model.response.NetworkCreateSavingsGoalResponseBody
import com.starllink.network.model.response.NetworkMoveMoneyResponseBody
import com.starllink.network.retrofit.RetrofitFactory
import com.starllink.network.retrofit.RetrofitStarlinkNetworkApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import retrofit2.Response
import retrofit2.Retrofit
import java.util.Date

class StarlinkNetworkApiImplTest {

    private val retrofitStarlinkNetworkApi = mock<RetrofitStarlinkNetworkApi>()
    private val retrofit = mock<Retrofit> {
        on { create(RetrofitStarlinkNetworkApi::class.java) } doReturn retrofitStarlinkNetworkApi
    }
    private val retrofitFactory = mock<RetrofitFactory>() {
        on { create() } doReturn retrofit
    }

    private val starlinkNetworkApi = StarlinkNetworkApiImpl(
        retrofitFactory
    )

    @Test
    fun `verify get accounts method`() = runTest {
        val stubModel = NetworkAccounts(emptyList())
        whenever(retrofitStarlinkNetworkApi.getAccounts()).doReturn(stubModel)

        assertEquals(starlinkNetworkApi.getAccounts(), stubModel)
    }

    @Test
    fun `verify get feed items method`() = runTest {
        val stubModel = NetworkFeedItems(emptyList())
        val accountId = "accountId"
        val categoryId = "categoryId"
        val changesSince = Date()

        whenever(retrofitStarlinkNetworkApi.getFeedItems(accountId, categoryId, changesSince))
            .doReturn(stubModel)

        val res = starlinkNetworkApi.getFeedItems(accountId, categoryId, changesSince)
        assertEquals(res, stubModel)
    }

    @Test
    fun `verify create savings goal method`() = runTest {
        val accountId = "accountId"
        val requestBody = NetworkCreateSavingsGoalRequestBody(
            "test savings goal",
            "GBP"
        )

        val stubModel = NetworkCreateSavingsGoalResponseBody("savingsGoalUid")
        whenever(retrofitStarlinkNetworkApi.createSavingsGoal(accountId, requestBody)).doReturn(
            stubModel
        )

        val res = starlinkNetworkApi.createSavingsGoal(accountId, requestBody)
        assertEquals(res, stubModel)
    }

    @Test
    fun `verify get savings goal method`() = runTest {
        val accountId = "accountId"

        val stubModel = NetworkSavingsGoals(listOf(NetworkSavingsGoal("savingsGoalUid")))
        whenever(retrofitStarlinkNetworkApi.getSavingsGoal(accountId)).doReturn(
            stubModel
        )

        val res = starlinkNetworkApi.getSavingsGoal(accountId)
        assertEquals(res, stubModel)
    }

    @Test
    fun `verify delete savings goal method`() = runTest {
        val accountId = "accountId"
        val savingsGoalUid = "savingsGoalUid"
        val response = mock<Response<Unit>>()
        whenever(retrofitStarlinkNetworkApi.deleteSavingsGoal(accountId, savingsGoalUid)).doReturn(
            response
        )

        starlinkNetworkApi.deleteSavingsGoal(accountId, savingsGoalUid)
    }

    @Test
    fun `verify move money method`() = runTest {
        val accountId = "accountId"
        val savingsGoalUid = "savingsGoalUid"
        val transactionId = "transactionId"
        val amount = 123
        val request = NetworkMoveMoneyRequestBody(
            NetworkMoveMoneyAmount(amount, "GBP")
        )

        val response = NetworkMoveMoneyResponseBody(success = true)
        whenever(
            retrofitStarlinkNetworkApi.moveMoneyToSavings(
                accountUid = accountId,
                savingsGoalUid = savingsGoalUid,
                transactionId = transactionId,
                request = request
            )
        ).doReturn(response)

        val result = starlinkNetworkApi.moveMoneyToSavings(
            accountUid = accountId,
            savingsGoalUid = savingsGoalUid,
            transactionId = transactionId,
            request = request
        )
        assertEquals(response, result)
    }
}