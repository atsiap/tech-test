package com.starllink.network

import com.starllink.network.retrofit.DateConverter
import org.junit.Assert.assertEquals
import org.junit.Test
import java.sql.Timestamp

class DateConverterTest {

    private val dateConverter = DateConverter()

    @Test
    fun `verify date is being converted properly`() {
        val timestamp = Timestamp(TIMESTAMP)
        val result = dateConverter.convert(timestamp)

        assertEquals(result, DATE_STRING)
    }

    private companion object {
        const val TIMESTAMP = 1675078602957L
        const val DATE_STRING = "2023-01-30T11:36:42Z"
    }
}