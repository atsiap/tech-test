package com.starllink.network

import com.starllink.network.model.NetworkAccounts
import com.starllink.network.model.NetworkFeedItems
import com.starllink.network.model.NetworkSavingsGoals
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import com.starllink.network.model.response.NetworkCreateSavingsGoalResponseBody
import com.starllink.network.model.response.NetworkMoveMoneyResponseBody
import java.util.Date

interface StarlinkNetworkApi {

    suspend fun getAccounts(): NetworkAccounts

    suspend fun getFeedItems(
        accountUid: String,
        categoryUid: String,
        changesSince: Date
    ): NetworkFeedItems

    suspend fun createSavingsGoal(
        accountUid: String,
        createSavingsGoal: NetworkCreateSavingsGoalRequestBody
    ) : NetworkCreateSavingsGoalResponseBody

    suspend fun getSavingsGoal(accountUid: String): NetworkSavingsGoals

    suspend fun deleteSavingsGoal(accountUid: String, savingsGoalUid: String)

    suspend fun moveMoneyToSavings(
        accountUid: String,
        savingsGoalUid: String,
        transactionId: String,
        request: NetworkMoveMoneyRequestBody
    ) : NetworkMoveMoneyResponseBody
}