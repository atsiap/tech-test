package com.starllink.network.model

import kotlinx.serialization.Serializable

@Serializable
data class NetworkFeedItems(
    val feedItems: List<NetworkFeedItem>
)

@Serializable
data class NetworkFeedItem(val amount: NetworkAmount)

@Serializable
data class NetworkAmount(val minorUnits: Int)