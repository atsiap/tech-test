package com.starllink.network.model.request

import kotlinx.serialization.Serializable

@Serializable
data class NetworkMoveMoneyRequestBody(
    val amount: NetworkMoveMoneyAmount,
)

@Serializable
data class NetworkMoveMoneyAmount(
    val minorUnits: Int,
    val currency: String
)