package com.starllink.network.model.request

import kotlinx.serialization.Serializable

@Serializable
data class NetworkCreateSavingsGoalRequestBody(
    val name: String,
    val currency: String
)