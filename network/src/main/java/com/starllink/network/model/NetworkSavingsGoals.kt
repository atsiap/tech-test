package com.starllink.network.model

import kotlinx.serialization.Serializable

@Serializable
data class NetworkSavingsGoals(val savingsGoalList: List<NetworkSavingsGoal>)

@Serializable
data class NetworkSavingsGoal(val savingsGoalUid: String)