package com.starllink.network.model

import kotlinx.serialization.Serializable

@Serializable
data class NetworkAccounts(val accounts: List<NetworkAccount>)