package com.starllink.network.model.response

import kotlinx.serialization.Serializable

@Serializable
data class NetworkCreateSavingsGoalResponseBody(
    val savingsGoalUid: String
)