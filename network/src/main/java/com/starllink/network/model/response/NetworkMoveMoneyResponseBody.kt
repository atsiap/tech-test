package com.starllink.network.model.response

import kotlinx.serialization.Serializable

@Serializable
data class NetworkMoveMoneyResponseBody(val success: Boolean)