package com.starllink.network.model

import kotlinx.serialization.Serializable

@Serializable
data class NetworkAccount(
    val accountUid: String,
    val defaultCategory: String,
)