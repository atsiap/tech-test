package com.starllink.network.retrofit

import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type
import java.util.Date

/**
 * [Retrofit] Converter Factory which converts a date to the specified format.
 * This is used for the query parameter of type [Date]
 */
internal class DateConverterFactory(
    private val dateConverter: DateConverter = DateConverter()
) : Converter.Factory() {

    override fun stringConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<*, String>? {
        return if (type == Date::class.java) {
            dateConverter
        } else {
            super.stringConverter(type, annotations, retrofit)
        }
    }
}