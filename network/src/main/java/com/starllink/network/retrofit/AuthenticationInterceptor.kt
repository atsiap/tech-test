package com.starllink.network.retrofit

import com.starllink.network.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

internal class AuthenticationInterceptor : Interceptor {

    private val token = "$BEARER ${BuildConfig.STARLINK_AUTH_TOKEN}"

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        val authenticatedRequest: Request = request.newBuilder()
            .header("Authorization", token)
            .header("Accept", "application/json")
            .build()
        return chain.proceed(authenticatedRequest)
    }

    private companion object {
        const val BEARER = "Bearer"
    }
}