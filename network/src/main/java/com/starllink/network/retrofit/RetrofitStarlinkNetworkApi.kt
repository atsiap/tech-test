package com.starllink.network.retrofit

import com.starllink.network.model.NetworkAccounts
import com.starllink.network.model.NetworkFeedItems
import com.starllink.network.model.NetworkSavingsGoals
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import com.starllink.network.model.response.NetworkCreateSavingsGoalResponseBody
import com.starllink.network.model.response.NetworkMoveMoneyResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import java.util.Date

internal interface RetrofitStarlinkNetworkApi {

    @GET(value = "accounts")
    suspend fun getAccounts(): NetworkAccounts

    @GET(value = "feed/account/{accountUid}/category/{categoryUid}")
    suspend fun getFeedItems(
        @Path("accountUid") accountUid: String,
        @Path("categoryUid") categoryUid: String,
        @Query("changesSince") changesSince: Date
    ): NetworkFeedItems

    @PUT(value = "account/{accountUid}/savings-goals")
    suspend fun createSavingsGoal(
        @Path("accountUid") accountUid: String,
        @Body createSavingsGoal: NetworkCreateSavingsGoalRequestBody
    ): NetworkCreateSavingsGoalResponseBody

    @GET(value = "account/{accountUid}/savings-goals")
    suspend fun getSavingsGoal(@Path("accountUid") accountUid: String): NetworkSavingsGoals

    @DELETE(value = "account/{accountUid}/savings-goals/{savingsGoalUid}")
    suspend fun deleteSavingsGoal(
        @Path("accountUid") accountUid: String,
        @Path("savingsGoalUid") savingsGoalUid: String
    ) : Response<Unit>

    @PUT(value = "account/{accountUid}/savings-goals/{savingsGoalUid}/add-money/{transactionId}")
    suspend fun moveMoneyToSavings(
        @Path("accountUid")  accountUid: String,
        @Path("savingsGoalUid") savingsGoalUid: String,
        @Path("transactionId") transactionId: String,
        @Body request: NetworkMoveMoneyRequestBody
    ) : NetworkMoveMoneyResponseBody
}