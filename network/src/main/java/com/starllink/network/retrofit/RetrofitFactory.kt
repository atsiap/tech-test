package com.starllink.network.retrofit

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.starllink.network.BuildConfig
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Inject

internal class RetrofitFactory @Inject constructor() {

    fun create(): Retrofit {
        val json = Json { ignoreUnknownKeys = true }
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(
                OkHttpClient.Builder().also { builder ->
                    createInterceptors().forEach(builder::addInterceptor)
                }.build()
            )
            .addConverterFactory(
                @OptIn(ExperimentalSerializationApi::class)
                json.asConverterFactory("application/json".toMediaType())
            )
            .addConverterFactory(DateConverterFactory())
            .build()
    }

    private fun createInterceptors() = when (BuildConfig.DEBUG) {
        true -> listOf(
            AuthenticationInterceptor(),
            HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            }
        )

        false -> listOf(AuthenticationInterceptor())
    }

    private companion object {
        const val BASE_URL = "https://api-sandbox.starlingbank.com/api/v2/"
    }
}