package com.starllink.network.retrofit

import retrofit2.Converter
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

internal class DateConverter() : Converter<Date, String> {

    /**
     * SimpleDateFormat is not thread safe, make sure we create a new instance if the
     * retrofit has invoked this on a different thread
     */
    private val dateFormat = object : ThreadLocal<DateFormat>() {

        override fun initialValue(): DateFormat? {
            return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault())
        }
    }

    override fun convert(value: Date): String? {
        return dateFormat.get()!!.format(value)
    }
}