package com.starllink.network

import com.starllink.network.model.NetworkAccounts
import com.starllink.network.model.NetworkFeedItems
import com.starllink.network.model.NetworkSavingsGoals
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import com.starllink.network.model.response.NetworkCreateSavingsGoalResponseBody
import com.starllink.network.model.response.NetworkMoveMoneyResponseBody
import com.starllink.network.retrofit.RetrofitFactory
import com.starllink.network.retrofit.RetrofitStarlinkNetworkApi
import java.util.Date
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class StarlinkNetworkApiImpl @Inject internal constructor(
    private val retrofitFactory: RetrofitFactory
) : StarlinkNetworkApi {

    private val retrofitStarlinkNetworkApi by lazy {
        val retrofit = retrofitFactory.create()
        retrofit.create(RetrofitStarlinkNetworkApi::class.java)
    }

    override suspend fun getAccounts(): NetworkAccounts {
        return retrofitStarlinkNetworkApi.getAccounts()
    }

    override suspend fun getFeedItems(
        accountUid: String,
        categoryUid: String,
        changesSince: Date
    ): NetworkFeedItems {
        return retrofitStarlinkNetworkApi.getFeedItems(accountUid, categoryUid, changesSince)
    }

    override suspend fun createSavingsGoal(
        accountUid: String,
        createSavingsGoal: NetworkCreateSavingsGoalRequestBody
    ): NetworkCreateSavingsGoalResponseBody {
        return retrofitStarlinkNetworkApi.createSavingsGoal(accountUid, createSavingsGoal)
    }

    override suspend fun getSavingsGoal(accountUid: String): NetworkSavingsGoals {
        return retrofitStarlinkNetworkApi.getSavingsGoal(accountUid)
    }

    override suspend fun deleteSavingsGoal(accountUid: String, savingsGoalUid: String) {
        return retrofitStarlinkNetworkApi.deleteSavingsGoal(accountUid, savingsGoalUid).let {}
    }

    override suspend fun moveMoneyToSavings(
        accountUid: String,
        savingsGoalUid: String,
        transactionId: String,
        request: NetworkMoveMoneyRequestBody
    ): NetworkMoveMoneyResponseBody {
        return retrofitStarlinkNetworkApi.moveMoneyToSavings(
            accountUid,
            savingsGoalUid,
            transactionId,
            request
        )
    }
}