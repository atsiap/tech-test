package com.starllink.network.di

import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.StarlinkNetworkApiImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface NetworkModule {

    @Binds
    fun bindsTopicRepository(
        starlinkNetworkApi: StarlinkNetworkApiImpl
    ): StarlinkNetworkApi
}