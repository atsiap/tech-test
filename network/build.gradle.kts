import com.starlink.techtest.plugins.BuildConfigUtil
import com.starlink.techtest.plugins.BuildConfigUtil.requiredBuildConfig

plugins {
    id("com.starlink.techtest.android.library")
    id("com.starlink.techtest.android.hilt")
    id("org.jetbrains.kotlin.plugin.serialization")
}

android {
    namespace = "com.starllink.network"

    defaultConfig {
        requiredBuildConfig("STARLINK_AUTH_TOKEN", this)
    }
}

dependencies {
    implementation(libs.okhttp.logging)
    implementation(libs.retrofit.core)
    implementation(libs.retrofit.kotlin.serialization)
    implementation(libs.kotlin.coroutines.android)
    implementation(libs.kotlin.serialization.json)

    testImplementation(libs.kotlin.coroutines.test)
    testImplementation(libs.kotlin.mockito)
    testImplementation("junit:junit:4.13.2")
}