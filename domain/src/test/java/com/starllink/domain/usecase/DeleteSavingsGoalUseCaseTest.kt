package com.starllink.domain.usecase

import com.starllink.network.StarlinkNetworkApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.IOException

class DeleteSavingsGoalUseCaseTest {

    private val starlinkNetworkApi = mock<StarlinkNetworkApi>()
    private val useCase = DeleteSavingsGoalUseCase(starlinkNetworkApi)

    @Test
    fun `verify usecase completed successfully`() = runTest {
        val accountId = "accountId"
        val savingsGoalUid = "savingsGoalUid"

        whenever(starlinkNetworkApi.deleteSavingsGoal(accountId, savingsGoalUid)).doReturn(
            Unit
        )

        useCase.execute(DeleteSavingsGoalUseCase.Params(accountId, savingsGoalUid))
    }

    @Test
    fun `verify error occur during fetching`() = runTest {
        val accountId = "accountId"
        val savingsGoalUid = "savingsGoalUid"
        val exception = IOException("error")

        whenever(starlinkNetworkApi.deleteSavingsGoal(accountId, savingsGoalUid))
            .thenAnswer { throw exception }

        assertThrows(exception) {
            useCase.execute(DeleteSavingsGoalUseCase.Params(accountId, savingsGoalUid))
        }
    }
}