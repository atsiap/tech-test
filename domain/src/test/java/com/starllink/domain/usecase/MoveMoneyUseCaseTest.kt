package com.starllink.domain.usecase

import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.request.NetworkMoveMoneyAmount
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import com.starllink.network.model.response.NetworkMoveMoneyResponseBody
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.IOException

class MoveMoneyUseCaseTest {

    private val starlinkNetworkApi = mock<StarlinkNetworkApi>()
    private val errorProvider = mock<MoneyUseCaseErrorProvider>()
    private val useCase = MoveMoneyUseCase(starlinkNetworkApi, errorProvider)

    private val accountId = "accountId"
    private val savingsGoalUid = "savingsGoalUid"
    private val transactionId = "transactionId"
    private val amount = 123
    private val request = NetworkMoveMoneyRequestBody(
        NetworkMoveMoneyAmount(amount, "GBP")
    )

    @Test
    fun `verify usecase completed successfully`() = runTest {
        val response = NetworkMoveMoneyResponseBody(success = true)

        whenever(
            starlinkNetworkApi.moveMoneyToSavings(accountId, savingsGoalUid, transactionId, request)
        ).doReturn(response)

        useCase.execute(
            MoveMoneyUseCase.Params(accountId, savingsGoalUid, transactionId, amount)
        )
    }

    @Test
    fun `verify error occur during network call`() = runTest {
        val accountId = "accountId"
        val exception = IOException("error")

        whenever(
            starlinkNetworkApi.moveMoneyToSavings(accountId, savingsGoalUid, transactionId, request)
        ).thenAnswer { throw exception }

        assertThrows(exception) {
            useCase.execute(
                MoveMoneyUseCase.Params(accountId, savingsGoalUid, transactionId, amount)
            )
        }
    }

    @Test
    fun `verify error backend returned an error`() = runTest {
        val response = NetworkMoveMoneyResponseBody(success = false)
        val error = IllegalStateException("Failed to move money")

        whenever(errorProvider.createError()).thenReturn(error)
        whenever(
            starlinkNetworkApi.moveMoneyToSavings(accountId, savingsGoalUid, transactionId, request)
        ).doReturn(response)

        assertThrows(error) {
            useCase.execute(
                MoveMoneyUseCase.Params(accountId, savingsGoalUid, transactionId, amount)
            )
        }
    }
}