package com.starllink.domain.usecase

import org.junit.Test

class RoundUpCalculatorTest {

    private val roundUpCalculator = RoundUpCalculator()

    @Test
    fun `verify calculator`() {
        val feedItemAmounts = listOf(435, 520, 87)

        val result = roundUpCalculator.calculate(feedItemAmounts)
        assert(result == 158)
    }
}