package com.starllink.domain.usecase

import com.starllink.domain.models.RoundAmount
import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.NetworkAmount
import com.starllink.network.model.NetworkFeedItem
import com.starllink.network.model.NetworkFeedItems
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.IOException
import java.util.Date
import kotlin.coroutines.cancellation.CancellationException

class RoundUpUseCaseTest {

    private val starlinkNetworkApi = mock<StarlinkNetworkApi>()
    private val dateFormatter = mock<TimeMachineDateFormatter>()
    private val roundUpCalculator = mock<RoundUpCalculator>()

    private val useCase = RoundUpUseCase(
        starlinkNetworkApi = starlinkNetworkApi,
        dateFormatter = dateFormatter,
        roundUpCalculator = roundUpCalculator
    )

    @Test
    fun `verify usecase completed successfully`() = runTest {
        val date = Date()
        val accountId = "accountId"
        val categoryId = "categoryId"

        whenever(dateFormatter.provideCurrentTime()).doReturn(date)

        whenever(starlinkNetworkApi.getFeedItems(accountId, categoryId, date)).doReturn(FIXTURES)
        whenever(dateFormatter.minusWeek(date, 1)).doReturn(date)
        whenever(roundUpCalculator.calculate(listOf(1, 2, 3))).doReturn(158)

        val result = useCase.execute(RoundUpUseCase.Params(accountId, categoryId))

        assertEquals(RoundAmount(158), result)
    }

    @Test
    fun `verify error occur during fetching`() = runTest {
        val exception = IOException("error")
        val date = Date()

        whenever(dateFormatter.provideCurrentTime()).doReturn(date)
        whenever(dateFormatter.minusWeek(date, 1)).doReturn(date)
        whenever(starlinkNetworkApi.getFeedItems(any(), any(), any())).thenAnswer { throw exception }
        assertThrows(exception) { useCase.execute(RoundUpUseCase.Params("", "")) }
    }

    private companion object {
        val FIXTURES = NetworkFeedItems(
            feedItems = listOf(
                NetworkFeedItem(NetworkAmount(1)),
                NetworkFeedItem(NetworkAmount(2)),
                NetworkFeedItem(NetworkAmount(3))
            )
        )
    }
}