package com.starllink.domain.usecase

import org.junit.Assert
import org.junit.Test
import java.sql.Timestamp

class TimeMachineDateFormatterTest {

    private val formatter = TimeMachineDateFormatter()

    @Test
    fun `verify date is being converted properly`() {
        val timestamp = Timestamp(TIMESTAMP)
        val result = formatter.minusWeek(timestamp, 1)

        Assert.assertEquals(EXPECTED_TIMESTAMP, result.time)
    }

    private companion object {
        const val EXPECTED_TIMESTAMP = 1674473802957L
        const val TIMESTAMP = 1675078602957L
    }
}