package com.starllink.domain.usecase

import com.starllink.domain.models.SavingsGoal
import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import com.starllink.network.model.response.NetworkCreateSavingsGoalResponseBody
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.IOException

class CreateSavingsGoalUseCaseTest {

    private val starlinkNetworkApi = mock<StarlinkNetworkApi>()
    private val useCase = CreateSavingsGoalUseCase(starlinkNetworkApi)

    @Test
    fun `verify usecase completed successfully`() = runTest {
        val accountId = "accountId"
        val savingsGoalUid = "savingsGoalUid"
        val savingsGoal = SavingsGoal(savingsGoalUid, true)

        whenever(starlinkNetworkApi.createSavingsGoal(accountId, REQUEST_FIXTURE)).doReturn(
            NetworkCreateSavingsGoalResponseBody(savingsGoalUid)
        )

        val result = useCase.execute(accountId)

        assertEquals(savingsGoal, result)
    }

    @Test
    fun `verify error occur during fetching`() = runTest {
        val accountId = "accountId"
        val exception = IOException("error")

        whenever(starlinkNetworkApi.createSavingsGoal(accountId, REQUEST_FIXTURE))
            .thenAnswer { throw exception }

        assertThrows(exception) { useCase.execute(accountId) }
    }

    private companion object {
        val REQUEST_FIXTURE = NetworkCreateSavingsGoalRequestBody(
            name = "test savings goal",
            currency = "GBP"
        )
    }
}