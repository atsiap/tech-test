package com.starllink.domain.usecase

import org.junit.Assert
import kotlin.coroutines.cancellation.CancellationException

internal suspend fun <T> assertThrows(exception: Throwable, block: suspend () -> T) {
    var error: Throwable? = null
    try {
        block()
    } catch (e: CancellationException) {
        throw e
    } catch (t: Throwable) {
        error = t
    }

    Assert.assertEquals(exception, error)
}