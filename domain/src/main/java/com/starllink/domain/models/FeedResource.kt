package com.starllink.domain.models

data class FeedResource(
    val accountUid: String,
    val defaultCategory: String,
    val roundAmount: RoundAmount,
    val savingsGoal: SavingsGoal?
)