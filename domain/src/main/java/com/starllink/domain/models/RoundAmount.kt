package com.starllink.domain.models

data class RoundAmount(val amount: Int)