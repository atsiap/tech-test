package com.starllink.domain.models

data class SavingsGoal(
    val savingsGoalUid: String,
    /**
     * In case there are multiple savings goal, we only pick the first one.
     * This is being done for the purpose of the tech test. This flag can be used
     * to notify the user about it
     */
    val isSingle: Boolean
)