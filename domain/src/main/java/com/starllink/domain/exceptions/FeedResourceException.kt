package com.starllink.domain.exceptions

import com.starllink.domain.usecase.FeedUseCase

/**
 * This exception is being thrown if there is a precondition violation, when we are invoking
 * the usecase [FeedUseCase]
 */
class FeedResourceException(
    val errorType: ErrorType,
    cause: Throwable? = null
) : IllegalStateException(cause) {

    enum class ErrorType {
        /**
         * Used when there is no active account
         */
        ACCOUNT_NOT_FOUND,
        /**
         * Failure during the fetch of the account
         */
        ACCOUNT_FAILED,

        /**
         * Failure during the fetch of feed items
         */
        ROUND_UP_FAILED,

        /**
         * Failure during the fetch of savings goal
         */
        SAVINGS_GOAL_FAILED
    }
}

