package com.starllink.domain.core

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

/**
 * Base UseCase class which is being used by single result operations.
 * It either emits a result with type [Output] or emits an error with type [Exception]
 */
interface SuspendingUseCase<Input, Output> {

    suspend fun execute(input: Input): Output
}

/**
 * Utility method being used to convert a suspending function to a flow
 */
fun <Input, Output> SuspendingUseCase<Input, Output>.enqueue(input: Input): Flow<Output> {
    return suspend { execute(input) }.asFlow()
}
