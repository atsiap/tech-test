package com.starllink.domain.usecase

import com.starllink.domain.core.SuspendingUseCase
import com.starllink.domain.models.SavingsGoal
import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.request.NetworkCreateSavingsGoalRequestBody
import javax.inject.Inject

class CreateSavingsGoalUseCase @Inject constructor(
    private val starlinkNetworkApi: StarlinkNetworkApi,
) : SuspendingUseCase<String, SavingsGoal> {

    override suspend fun execute(input: String): SavingsGoal {
        return starlinkNetworkApi.createSavingsGoal(
            accountUid = input,
            createSavingsGoal = NetworkCreateSavingsGoalRequestBody(
                name = "test savings goal",
                currency = "GBP"
            )
        ).let { SavingsGoal(it.savingsGoalUid, true) }
    }
}