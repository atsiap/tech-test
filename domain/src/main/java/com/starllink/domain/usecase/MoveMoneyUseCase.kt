package com.starllink.domain.usecase

import com.starllink.domain.core.SuspendingUseCase
import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.request.NetworkMoveMoneyAmount
import com.starllink.network.model.request.NetworkMoveMoneyRequestBody
import javax.inject.Inject

class MoveMoneyUseCase @Inject internal constructor(
    private val starlinkNetworkApi: StarlinkNetworkApi,
    private val errorProvider: MoneyUseCaseErrorProvider
) : SuspendingUseCase<MoveMoneyUseCase.Params, Unit> {

    data class Params(
        val accountUid: String,
        val savingsGoalUid: String,
        val transactionId: String,
        val amount: Int
    )

    override suspend fun execute(input: Params) {
        val request = NetworkMoveMoneyRequestBody(
            NetworkMoveMoneyAmount(input.amount, "GBP")
        )

        val response = starlinkNetworkApi.moveMoneyToSavings(
                accountUid = input.accountUid,
                savingsGoalUid = input.savingsGoalUid,
                transactionId = input.transactionId,
                request = request
        )

        if (!response.success) {
            throw errorProvider.createError()
        }
    }
}