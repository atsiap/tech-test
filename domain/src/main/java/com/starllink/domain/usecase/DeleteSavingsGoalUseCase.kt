package com.starllink.domain.usecase

import com.starllink.domain.core.SuspendingUseCase
import com.starllink.network.StarlinkNetworkApi
import javax.inject.Inject

class DeleteSavingsGoalUseCase @Inject constructor(
    private val starlinkNetworkApi: StarlinkNetworkApi,
) : SuspendingUseCase<DeleteSavingsGoalUseCase.Params, Unit> {

    data class Params(
        val accountUid: String,
        val savingsGoalUid: String
    )

    override suspend fun execute(input: Params): Unit {
        return starlinkNetworkApi.deleteSavingsGoal(
            accountUid = input.accountUid,
            savingsGoalUid = input.savingsGoalUid
        )
    }
}