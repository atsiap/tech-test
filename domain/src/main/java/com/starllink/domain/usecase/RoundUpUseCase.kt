package com.starllink.domain.usecase

import com.starllink.domain.core.SuspendingUseCase
import com.starllink.domain.models.RoundAmount
import com.starllink.network.StarlinkNetworkApi
import javax.inject.Inject

internal class RoundUpUseCase @Inject internal constructor(
    private val starlinkNetworkApi: StarlinkNetworkApi,
    private val dateFormatter: TimeMachineDateFormatter,
    private val roundUpCalculator: RoundUpCalculator
) : SuspendingUseCase<RoundUpUseCase.Params, RoundAmount> {

    data class Params(
        val accountUid: String,
        val defaultCategory: String
    )

    override suspend fun execute(input: Params): RoundAmount {
        val feedItems = starlinkNetworkApi.getFeedItems(
            input.accountUid,
            input.defaultCategory,
            dateFormatter.minusWeek(dateFormatter.provideCurrentTime(), 1)
        ).feedItems

        val amounts = feedItems.map { it.amount.minorUnits }

        return RoundAmount(roundUpCalculator.calculate(amounts))
    }
}