package com.starllink.domain.usecase

import com.starllink.domain.core.SuspendingUseCase
import com.starllink.domain.exceptions.FeedResourceException
import com.starllink.domain.exceptions.FeedResourceException.ErrorType
import com.starllink.domain.models.FeedResource
import com.starllink.domain.models.SavingsGoal
import com.starllink.domain.utils.suspendTry
import com.starllink.network.StarlinkNetworkApi
import com.starllink.network.model.NetworkAccount
import com.starllink.network.model.NetworkSavingsGoal
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class FeedUseCase @Inject internal constructor(
    private val starlinkNetworkApi: StarlinkNetworkApi,
    private val roundUpUseCase: RoundUpUseCase
) : SuspendingUseCase<Unit, FeedResource> {

    override suspend fun execute(input: Unit): FeedResource {
        val accountsResponse = suspendTry(starlinkNetworkApi::getAccounts) {
            FeedResourceException(ErrorType.ACCOUNT_FAILED, it)
        }

        // For the purpose of the tech test we assume that there is only 1 account.
        // For the savings goals there is a hint that there might be multiple
        val account = accountsResponse.accounts.firstOrNull() ?: throw FeedResourceException(
            ErrorType.ACCOUNT_NOT_FOUND
        )

        val (roundAmount, savingsGoal) = getAmountAndSavingsGoal(account)

        return FeedResource(
            accountUid = account.accountUid,
            defaultCategory = account.defaultCategory,
            roundAmount = roundAmount,
            savingsGoal = savingsGoal
        )
    }

    /**
     * Fetch both of them in parallel
     */
    private suspend fun getAmountAndSavingsGoal(account: NetworkAccount) = Pair(
        withContext(coroutineContext) { getRoundUpAmount(account) },
        withContext(coroutineContext) { getSavingsGoal(account) }
    )

    private suspend fun getSavingsGoal(account: NetworkAccount): SavingsGoal? {
        val savingsGoals = suspendTry(
            {
                starlinkNetworkApi.getSavingsGoal(account.accountUid)
            },
            { FeedResourceException(ErrorType.SAVINGS_GOAL_FAILED, it) }
        )

        val savingsGoalList = savingsGoals.savingsGoalList
        return if (savingsGoalList.isEmpty()) {
            null
        } else {
            /**
             * In case there are multiple savings goal, we only pick the first one.
             * This is being done for the purpose of the tech test. This flag can be used
             * to notify the user about it
             */
            savingsGoalList.first().toDomain(savingsGoalList.size == 1)
        }
    }

    private suspend fun getRoundUpAmount(account: NetworkAccount) = suspendTry(
        {
            roundUpUseCase.execute(
                RoundUpUseCase.Params(
                    account.accountUid,
                    account.defaultCategory
                )
            )
        },
        { FeedResourceException(ErrorType.ROUND_UP_FAILED, it) }
    )

    private fun NetworkSavingsGoal.toDomain(isSingle: Boolean) = SavingsGoal(
        savingsGoalUid,
        isSingle
    )
}

