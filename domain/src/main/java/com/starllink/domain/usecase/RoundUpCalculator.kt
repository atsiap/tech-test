package com.starllink.domain.usecase

import javax.inject.Inject
import kotlin.math.ceil

internal class RoundUpCalculator @Inject constructor() {

    fun calculate(amounts: List<Int>): Int {
        return amounts.map {
            val ceilNumber = (ceil(it / 100.0) * 100.0).toInt()
            ceilNumber - it
        }.sumOf { it }
    }
}