package com.starllink.domain.usecase

import javax.inject.Inject

internal class MoneyUseCaseErrorProvider @Inject constructor() {

    fun createError() = IllegalStateException("Failed to move money")
}