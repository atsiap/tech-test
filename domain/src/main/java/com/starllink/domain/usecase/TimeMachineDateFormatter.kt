package com.starllink.domain.usecase

import java.util.Calendar
import java.util.Date
import javax.inject.Inject

/**
 * Utility class which returns a [Date] back in time.
 */
internal class TimeMachineDateFormatter @Inject constructor() {

    private val calendar by lazy { Calendar.getInstance() }

    fun minusWeek(date: Date, offset: Int): Date {
        requireNotNull(offset > 0) {
            "An integer bigger than 0 must be passed as an offset, you provided $offset"
        }
        calendar.time = date
        calendar.add(Calendar.WEEK_OF_MONTH, -offset)
        return calendar.time
    }

    /**
     * Provides the current time. This could become a different class in reality.
     * just keeping into 1 for the tech test
     */
    fun provideCurrentTime(): Date = Date()
}