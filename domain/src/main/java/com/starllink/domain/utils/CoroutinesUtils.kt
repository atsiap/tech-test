package com.starllink.domain.utils

import kotlin.coroutines.cancellation.CancellationException
import kotlin.Result

/**
 * Attempts to execute the [block], returning a successful [T] if it succeeds, otherwise it throws
 * an exception, taking care not to break structured concurrency. If we use a simple try/catch
 * block, then we are going to shallow the cancellations, because we will never emit
 * the [CancellationException].
 */
internal suspend fun <T> suspendTry(
    block: suspend () -> T,
    errorMapper: (Throwable) -> Throwable = { it }
): T = try {
    block()
} catch (cancellationException: CancellationException) {
    throw cancellationException
} catch (exception: Exception) {
    throw errorMapper(exception)
}

/**
 * Executes the [block], returning a [Result] of type [T] taking care not to break
 * structured concurrency. If we use a simple try/catch block, then we are going to shallow
 * the cancellations, because we will never emit the [CancellationException].
 */
suspend fun <T> runCatchingSuspend(block: suspend () -> T): Result<T> = try {
    Result.success(block())
} catch (cancellationException: CancellationException) {
    throw cancellationException
} catch (exception: Exception) {
    Result.failure(exception)
}