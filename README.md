# README

## Architecture

- Using MVVM and Jetpack ViewModel
- Using Kotlin coroutines.
- Using Dagger Hilt for dependency injection
- Using Jetpack Compose for the UI
- Feature modules
- Retrofit for the network calls


## Git History

The tech test is broken into small atomic commits. Some of the commit messages contains extra information where I am explaining the reasons behind the approach that I followed, e.g. [build-logic commit](https://gitlab.com/atsiap/tech-test/commit/9dfa57d5cadb00e0a68c8608e533c3db1f41fedb).  

## Build

- Reusable gradle plugins have implemented in [build-logic](build-logic/plugins/src/main/kotlin/com/starlink/techtest/plugins) module
- Reusable gradle plugins allows us to have very simple build.gradle files by eliminating the boilerplate and assisting on reusability of the code,
for example [domain/build.gradle](domain/build.gradle)
- For the gradle build system I am using the groovy format instead of Kotlin, unless its needed. Kotlin gradle files (kts) have several disadvantages 
such as slow build times, they break incremental complilation (on changes) etc. 
- By providing our own custom gradle plugins for the configuration, we are keeping the `build.gradle` file very small and simple, so the trade off
between kotlin and groovy is not that impactful. 
- Every module is an android library module in this project, although some of them (`network` & `domain`) could be a pure jvm module. In a real project 
we could convert them, here I only used the library module for simplicity reasons, since this is a tech test project. 
- Keep the [app](app) module as small as possible. This is generally a good practice, since it can also help in cases of white labeled apps in order to avoid multiple flavors.
The App module should be used as the main point where the configuration of the app has started.

## Testing

For this tech test I am only using jvm unit tests. On a real project we could also have e2e tests with espresso but also we could have screenshot tests.
I have skipped those here due to time constrains.

## UI

The UI design of this project is quite simple, despite this fact it demonstrates all of the existing states together with their errors.
For example: when we create a new savings goal, we display a label before the request starts and we hide the label when the request is finished.
A toast is being used to display the error message (if any)

There are 2 reasons behind this desision: 
1. From the description of the PDF of the technical task, I understood that the UI should be quite basic
2. Time constrains from my side

## User Flow

The user flows are the following: 

Scenario A:

1. App is opened -> fetching information -> display content
2. App is opened -> fetch information -> error occur -> display error and retry button

Scenario B (everything here continues from A1 scenario):

1. savings goal exist -> display button to move money to savings goal -> press button -> transfer successful -> show toast message 
2. savings goal exist -> display button to move money to savings goal -> press button -> transfer failed -> show toast message
3. savings goal exist -> display button to remove savings goal -> press remove button -> savings goal deleted succssfully -> hide transfer money button
4. savings goal exist -> display button to remove savings goal -> press remove button -> savings goal deleted failed -> show toast message

Scenario C: (everything here continues from A1 scenario):

1. savings goal does not exist -> display button to create savings goal -> press create button -> savings goal created successfully -> display transfer money button 
2. savings goal does not exist -> display button to create savings goal -> press create button -> savings goal created failed -> show toast message 

There is a video bellow attached to display this functionality.

![Screen Recording 2023-02-02 at 11 29 50](https://user-images.githubusercontent.com/470064/216315036-52fcf080-27e8-4b83-ae05-8a5c607dd814.gif)

[Download from Google drive as a video](https://drive.google.com/file/d/1muE76aUtil_0ToJEB9wg09A6nAxO0kf0/view?usp=share_link)

## How to build the project

1. Pass the starlink authentication token through the `STARLINK_AUTH_TOKEN` gradle property e.g. append the following on your `~/$Home/.gradle/gradle.properties`

```
STARLINK_AUTH_TOKEN = "my_auth_token" # Replace the token here with the real one
```
2. `./gradlew :app:assembleDebug`




